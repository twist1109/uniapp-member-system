import Vue from 'vue'
import App from './App'
import uView from 'uview-ui';
Vue.use(uView);
// const msg = (title, duration=1500, mask=false, icon='none')=>{
// 	//统一提示方便全局修改
// 	if(Boolean(title) === false){
// 		return;
// 	}
// 	uni.showToast({
// 		title,
// 		duration,
// 		mask,
// 		icon
// 	});
// }

Vue.config.productionTip = false
// 检查登录情况
Vue.prototype.checkLogin = function (backpage, backtype){
	var token = uni.getStorageSync('token')
	if (token == '') {
		uni.redirectTo({
			url:"../login/login?backpage=" + backpage + "&backtype=" + backtype
		});
		return false;
	}
	return [token];
}
// 退出
Vue.prototype.logout = function () {
	uni.removeStorage({
	    key: 'token'  
	})
	uni.removeStorage({
	    key: 'userInfo'  
	})
}
// 设置全局域名http://api.shop.com
// Vue.prototype.apiServer = 'http://api.fengliangjiuye.com/v1/'    
Vue.prototype.apiServer = 'http://api.shop.com/v1/'

App.mpType = 'app'
  
const app = new Vue({
    ...App
})
app.$mount()
